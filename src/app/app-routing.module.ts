import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from './components/register/register.component';
import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component';
import { BlogComponent } from './components/blog/blog.component';
import { AboutComponent } from './components/about/about.component';
import { AuthGuardService } from './auth-guard.service';
import { OrganizeComponent } from './components/organize/organize.component';


const routes: Routes = [
  {path:'',component: HomeComponent},
  {path:'signup',component: RegisterComponent, canActivate:[AuthGuardService]},
  {path:'login',component: LoginComponent, canActivate:[AuthGuardService]},
  {path:'profile',component: ProfileComponent},
  {path:'organize',component: OrganizeComponent},
  {path:'blog',component: BlogComponent},
  {path:'about',component: AboutComponent},


  {path:'**',redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
