import { Event } from './../model/event.model.';
import { Injectable } from '@angular/core';

@Injectable()
export class EventService{
    


    getAllEvents(){
        return this.allEvents;
    }

    getPremiumEvents(){
        let premiumEvents:Event[]=[];
        this.allEvents.forEach(event=>{
            if(event.type==="premium"){
              premiumEvents.push(event);
            }
          })
          return premiumEvents;
    }


    allEvents:Event[]=[
        {
            _id:"1",
            oid: "2",
            title: "Enstaze Inauguration",
            description: "Inauguration of Enstaze",
            location: "Kathmandu",
            url: "https://www.enstaze.com",
            contact: "9841700069",
            photo: "./assets/anEvent.jpg",
            type: 'normal',
            startDate: new Date('2020-01-25'),
            endDate:new Date('2020-01-30'),
            fee: 0,
            category: "Business"

        },
        {

            _id:"2",
            oid: "1",
            title: "Developer Conference",
            description: "Conference of Develoepers",
            location: "Kathmandu",
            url: "https://www.devCon.com",
            contact: "9841700069",
            photo: "./assets/anEvent1.jpg",
            type: 'normal',
            startDate: new Date('2020-01-25'),
            endDate:new Date('2020-01-30'),
            fee: 1000,
            category: "Technology"

        },
        {
            _id:"3",
            oid: "3",
            title: "Marketing Inauguration",
            description: "Discussion of Current Maketig Scenario of Enstaze",
            location: "Kathmandu",
            url: "https://www.enstaze.com",
            contact: "9841700069",
            photo: "./assets/anEvent2.jpg",
            type: 'normal',
            startDate: new Date('2020-01-25'),
            endDate:new Date('2020-01-30'),
            fee: 5000,
            category: "Marketing"
        },
        {
            _id:"4",
            oid: "5",
            title: "Honda Event",
            description: "Honda Ride around the town",
            location: "Kathmandu",
            url: "https://www.honda.com",
            contact: "9841700069",
            photo: "./assets/honda.jpeg",
            type: 'premium',
            startDate: new Date('2020-02-25'),
            endDate:new Date('2020-02-30'),
            fee: 0,
            category: "Adventure"
        },
        {
            _id:"5",
            oid: "4",
            title: "DJ Fest 2020",
            description: "Drink Dance and Dive",
            location: "Pokhara",
            url: "https://www.djfest.com",
            contact: "9841700069",
            photo: "./assets/djfest.jpeg",
            type: 'premium',
            startDate: new Date('2020-02-14'),
            endDate:new Date('2020-02-14'),
            fee: 5000,
            category: "Social"
        },
        {
            _id:"6",
            oid: "6",
            title: "Visit Nepal 2020",
            description: "",
            location: "Nepal",
            url: "https://visitnepal2020.com",
            contact: "9841700069",
            photo: "./assets/photo8.jpg",
            type: 'premium',
            startDate: new Date('2020-01-1'),
            endDate:new Date('2020-12-31'),
            fee: 0,
            category: "Tourism"
        }


    ];
}