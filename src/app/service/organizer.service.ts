import { Organizer } from './../model/organizer.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrganizerService {

  constructor() { }

  getAllOrganizer(){
    return this.AllOrganizer;
  }


  AllOrganizer: Organizer[]= [
    {
      oid: "1",
      oname: "Google inc",
      ophone: "9874444",
      oemail: "gogle@gmail.com",
      oaddress: "California"

    },
    {
      oid: "2",
      oname: "enstaze inc",
      ophone: "9841700069",
      oemail: "enstaze@gmail.com",
      oaddress: "kathmandu"
    },
    {
      oid: "3",
      oname: "Myriad",
      ophone: "98417887441",
      oemail: "myriad@gmail.com",
      oaddress: "Singapore"
    },
    {
      oid: "4",
      oname: "djfest",
      ophone: "98417887441",
      oemail: "djfest@gmail.com",
      oaddress: "djfest"
    },
    {
      oid: "5",
      oname: "Honda",
      ophone: "98417887441",
      oemail: "honda@gmail.com",
      oaddress: "Singapore"
    },
    {
      oid: "6",
      oname: "Nepal Government",
      ophone: "98417887441",
      oemail: "nepal@gov.com",
      oaddress: "Nepal"
    }

  ]
}
