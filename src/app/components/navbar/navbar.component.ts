import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/authentication.service';
import { Subscription } from 'rxjs';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  navbarOpen = false;
  isCollapsed = true;
  token = this.auth.getToken();
  username;
  isAuth: boolean;
  authSubscription: Subscription;
  userNameSubscription: Subscription;


  constructor(private router: Router, private auth: AuthenticationService, private db: AngularFireDatabase) {

  }

  async ngOnInit() {
    console.log('hello');
    await this.auth.authChange.subscribe(authStatus => {
      console.log("on in it: auth status : " + authStatus);
      this.isAuth = authStatus;

    });
    await this.auth.userChange.subscribe(newUser => {
      console.log("on in it user: " + newUser);
      this.username = newUser;
    })
    console.log(this.auth.getCurrentUser());

    this.auth.checkRole(localStorage.getItem('email'));




  }

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }

  logout() {
    this.auth.logout();
  }


  collapse() {
    this.isCollapsed = true;
  }



}
