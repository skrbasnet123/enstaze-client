import { OrganizerService } from './../../service/organizer.service';
import { Organizer } from './../../model/organizer.model';
import { Component, OnInit, Input, SimpleChanges, ChangeDetectionStrategy } from '@angular/core';
import { Event } from './../../model/event.model.';
import { EventService } from './../../service/event.service';
import { JsonPipe } from '@angular/common';

@Component({
  selector: 'app-all-events',
  templateUrl: './all-events.component.html',
  styleUrls: ['./all-events.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AllEventsComponent implements OnInit {
  allEvents: Event[];
  Organizer: string = "";
  allOrganizer: Organizer[];

  @Input() event: Event;

  constructor(eventService: EventService, organizerService: OrganizerService) {
    this.allEvents=eventService.getAllEvents();
    this.allOrganizer=organizerService.getAllOrganizer();
   }

  ngOnchanges(changes: SimpleChanges){
    // this.Organizer= this.getOrganizer(this.event.oid);
    console.log("Changes detected");
    
  }
  
  getOrganizer(oid){
     var org: Organizer;
     this.allOrganizer.forEach((organizer)=>{
       if(organizer.oid===oid){
         org=organizer;
         console.log(org);
         return org;
       }
     });
     return org;

  }

  ngOnInit() {
  }

}
