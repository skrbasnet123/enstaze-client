import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: '';
  password: '';
  btnDisabled= false;

  constructor(private auth: AuthenticationService, private router: Router) { }

  ngOnInit() {
  }

  validate(){
    if(this.email){
      if(this.password){
        return true;
      }else{
        window.alert("password not entered")
      }
    }else{
     window.alert("email not entered")
    }
  }

  login(){
    this.btnDisabled=true;
    if(this.validate()){
      this.auth.SignIn(this.email, this.password);
        
    }
    this.btnDisabled=false;
  }

}
