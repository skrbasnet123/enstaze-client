
import { Event } from './../../model/event.model.';
import { EventService } from './../../service/event.service';
import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-premium-events',
  templateUrl: './premium-events.component.html',
  styleUrls: ['./premium-events.component.css']
})
export class PremiumEventsComponent implements OnInit {
  allEvents: Event[];
  allPremiumEvents: Event[];

  constructor(eventService: EventService,
              config: NgbCarouselConfig) { 
    // this.allEvents=eventService.getAllEvents();
    this.allPremiumEvents = eventService.getPremiumEvents();
  

    
    config.interval = 5000;
    config.wrap = true;
    config.keyboard =  true;
    config.pauseOnHover= false;
    config.showNavigationIndicators= false
  }

  ngOnInit() {
    
  }
 
  

}
