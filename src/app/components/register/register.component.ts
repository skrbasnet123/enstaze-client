import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import 'firebase/database';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/authentication.service';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  name='';
  email = '';
  password = '';
  password1 = '';
  profilePicture= null;

  isOrganizer= false;
  orgName='';
  address='';
  verifiedOrganizer= false;

  btnDisabled=false;

  constructor(private db: AngularFireDatabase, private auth: AuthenticationService ) { }

  ngOnInit() {
  }

  validate() {
    if (this.name) {
      if (this.email) {
        if (this.password) {
          if (this.password1) {
            if (this.password === this.password1) {
              return true;
            } else {
              console.log("Password not matched");
            }
          } else {
            console.log("Confirm the password");
          }
        } else {
          console.log("password not entered");
        }
      } else {
        console.log("email not entered");
      }

    } else {
      console.log("name not entered")
    }
  }

  register(){
    console.log(this.name);
    this.btnDisabled=true;
    if(this.validate()){
      const user = {"name": this.name, "email": this.email,"isOrganizer": this.isOrganizer,"orgName": this.orgName, "address":this.address, "isVerifiedOrganizer": false};
      this.auth.SignUp(this.email, this.password, user);
      console.log("validate " +JSON.stringify(user) );
      // this.db.list('/users').push(user).then(result=>{console.log("successfully added: "+ result); window.alert("successfully registered")}).catch(err=>{console.log(err), window.alert(err)});;
      this.btnDisabled=false;
    }else{
      console.log("invalid signup");
      window.alert("invalid signup");
      this.btnDisabled=false;
    }

  }

  fileChange(event){
    console.log(event.target);
    this.profilePicture=event.target.files[0];
  }

}
