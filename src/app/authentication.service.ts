import { async } from '@angular/core/testing';
import { Observable, observable, Subject, BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { Router } from '@angular/router';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  user: any;

  authSuccessfully = this.getToken() ? true : false;
  updateObserver;

  updateUserName;



  userChange: Observable<string> = Observable.create((observer) => {
    observer.next(this.getUserName());
    //function to update auth status upon auth status changes
    this.updateUserName = function (updateName) {
      observer.next(updateName);
    }
  })

  authChange: Observable<boolean> = Observable.create((observer) => {
    observer.next(this.authSuccessfully);
    //function to update auth status from signin and logout
    this.updateObserver = function (updateValue) {
      observer.next(updateValue);
    }
  });


  constructor(private afAuth: AngularFireAuth, private router: Router, private db: AngularFireDatabase) { }

  // Sign up with email/password
  SignUp(email, password, user: any) {
    return this.afAuth.createUserWithEmailAndPassword(email, password)
      .then((result) => {
        result.user.updateProfile({ displayName: user.name }).then(() => {
          result.user.sendEmailVerification()
            .then(() => {
              console.log("email sent");
              console.log(result.user);
              this.db.list('/users').push(user)
                .then(result => {
                  console.log(`${user} is successfully registered`);
                  this.router.navigate(['/login']);
                }).catch(err => { console.log("error in pushing") });
            })
            .catch(err => { console.log("error in email verification") });
          window.alert("You have been successfully registered!");
        });
      }).catch((error) => {
        window.alert(error.message)
      })
  }

  // Sign in with email/password
  SignIn(email, password) {
    return this.afAuth.signInWithEmailAndPassword(email, password)
      .then(async (result) => {
        console.log('signed in successfully');
        await this.setToken();
        await this.updateObserver(true);
        await this.updateUserName(this.getUserName());
        this.authSuccessfully = true;
        this.router.navigate(['/']);
      }).catch((error) => {
        window.alert(error.message);
        this.router.navigate(['/login']);
      });
  }
  setToken() {
    this.afAuth.currentUser.then(user => {
      if (user) {
        this.user = user;
        console.log(JSON.parse(JSON.stringify(user)).stsTokenManager);
        console.log(JSON.parse(JSON.stringify(this.user)));
        const currentUser = JSON.parse(JSON.stringify(this.user));
        localStorage.setItem('token', currentUser.stsTokenManager.accessToken);
        localStorage.setItem('username', currentUser.displayName);
        localStorage.setItem('email', currentUser.email);
        this.checkRole(currentUser.email);
      }
    })
  }
  getToken() {
    return localStorage.getItem('token');
  }
  getUserName() {
    return localStorage.getItem('username');
  }

  getCurrentUser() {
    var userany;
    this.afAuth.currentUser.then(user => {
      userany = user;
    });
    return userany;
  }

  checkRole(email:string) {
    this.db.list('/users').valueChanges()
      .forEach(users => {
        users.forEach(user => {
          if (user['email'] === email) {
            if (user['isOrganizer']) {
              if (user['isVerifiedOrganizer']) {
                localStorage.setItem('role', "verifiedOrganizer");
              } else {
               localStorage.setItem('role',"pendingOrganizer");
              }
            } else {
              localStorage.setItem('role','normal');
            }
          }

        })
      })
  }



  isVerifiedOrganizer() {
    this.afAuth.currentUser.then(user => {
      if (user) {
        const userEmail = JSON.parse(JSON.stringify(user))
      }
    })
  }



  logout() {
    this.afAuth.signOut().then(() => {
      localStorage.clear();
      this.updateObserver(false);
      this.updateUserName('');
      this.authSuccessfully = false;
      this.router.navigate(['/login']);
    })


  }

}
