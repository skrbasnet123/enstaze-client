import { EventService } from './service/event.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PremiumEventsComponent } from './components/premium-events/premium-events.component';
import { AllEventsComponent } from './components/all-events/all-events.component';
import { FooterComponent } from './components/footer/footer.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { OrganizeComponent } from './components/organize/organize.component';
import { BlogComponent } from './components/blog/blog.component';
import { AboutComponent } from './components/about/about.component';
import { LoginComponent } from './components/login/login.component';
import { AngularFireModule} from '@angular/fire';
import { environment } from '../environments/environment';
import { FormsModule } from '@angular/forms';
import { AngularFireDatabaseModule, AngularFireDatabase } from '@angular/fire/database';
import { AuthenticationService } from './authentication.service';
import { AngularFireAuth } from '@angular/fire/auth';




@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    PremiumEventsComponent,
    AllEventsComponent,
    FooterComponent,
    RegisterComponent,
    HomeComponent,
    ProfileComponent,
    OrganizeComponent,
    BlogComponent,
    AboutComponent,
    LoginComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    AngularFireModule.initializeApp(environment.firebase),
    FormsModule,
    AngularFireDatabaseModule
  ],
  providers: [EventService, AngularFireDatabase, AuthenticationService, AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
