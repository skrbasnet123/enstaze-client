import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(private router:Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):boolean{
  
    if(localStorage.getItem('token')){
      this.router.navigate(['/']);
      return false; //means navigation to signup or login is cancelled if token found , instead redirected to ['/]
    }else{
      return true; //means navigation can continue to signup or login ie where can Activate is implemented in approuting module
    }
  }
}
