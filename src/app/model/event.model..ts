export class Event{
    _id: string;
    oid: string; //Organizer id
    title: string;
    description: string;
    location: string;
    url: string;
    contact?: string;
    photo?: string;
    type: 'premium'| 'featured'|'normal';
    startDate: Date;
    endDate?:Date;
    fee: number;
    category: string
}